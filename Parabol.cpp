#include "Parabol.h"
#include <cmath>

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	//draw 2 points
	SDL_RenderDrawPoint(ren, xc+x, y+yc);
	SDL_RenderDrawPoint(ren, xc-x, y+yc);
}
void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	A=std::abs(A);
	int x=0, y=0, p=1-A;
	while (x<A) {
		Draw2Points(xc,yc,x,y,ren);
		x++;
		p += 2*x +1;
		if (p>0) {
			y++;
			p -= 2*A;
		}
	}
	int w,h;
	SDL_GetRendererOutputSize(ren,&w,&h);
	while (y+yc<=h-1) {
		Draw2Points(xc,yc,x,y,ren);
		y++;
		p -= 2*A;
		if (p<0) {
			x++;
			p += 2*x;
		}
	}
}

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	A=-std::abs(A);
	int x=0, y=0, p=1+A;
	while (x<-A) {
		Draw2Points(xc,yc,x,y,ren);
		x++;
		p += 2*x+1;
		if (p>0) {
			y--;
			p += 2*A;
		}
	}
	while (y>=-yc) {
		Draw2Points(xc,yc,x,y,ren);
		y--;
		p += 2*A;
		if (p<0) {
			x++;
			p += 2*x;
		}
	}
}
