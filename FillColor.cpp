#include "FillColor.h"
#include <iostream>
#include <stack>
#include <utility>
#include <algorithm>
using namespace std;

#define INT_SIZE sizeof(Uint32) * 8 /* Integer size in bits */

int findHighestBitSet(Uint32 num)
{
    int order = 0, i;

    /* Iterate over each bit of integer */
    for(i=0; i<INT_SIZE; i++)
    {
        /* If current bit is set */
        if((num>>i) & 1)
            order = i;
    }

    return order;
}

//Get color of a pixel
SDL_Color getPixelColor(Uint32 pixel_format, Uint32 pixel)
{
    SDL_PixelFormat* fmt = SDL_AllocFormat(pixel_format);

    Uint32 temp;
    Uint8 red, green, blue, alpha;

    //Check if pixel is a 32-bit integer
    if (findHighestBitSet(pixel) == 31)
    {
        /* Get Alpha component */
        temp = pixel & fmt->Amask;  /* Isolate alpha component */
        temp = temp >> fmt->Ashift; /* Shift it down to 8-bit */
        temp = temp << fmt->Aloss;  /* Expand to a full 8-bit number */
        alpha = (Uint8)temp;
    } else {
        alpha = 255;
    }
    
    /* Get Red component */
    temp = pixel & fmt->Rmask;  /* Isolate red component */
    temp = temp >> fmt->Rshift; /* Shift it down to 8-bit */
    temp = temp << fmt->Rloss;  /* Expand to a full 8-bit number */
    red = (Uint8)temp;

    /* Get Green component */
    temp = pixel & fmt->Gmask;  /* Isolate green component */
    temp = temp >> fmt->Gshift; /* Shift it down to 8-bit */
    temp = temp << fmt->Gloss;  /* Expand to a full 8-bit number */
    green = (Uint8)temp;

    /* Get Blue component */
    temp = pixel & fmt->Bmask;  /* Isolate blue component */
    temp = temp >> fmt->Bshift; /* Shift it down to 8-bit */
    temp = temp << fmt->Bloss;  /* Expand to a full 8-bit number */
    blue = (Uint8)temp;

    /* Get Alpha component */
    temp = pixel & fmt->Amask;  /* Isolate alpha component */
    temp = temp >> fmt->Ashift; /* Shift it down to 8-bit */
    temp = temp << fmt->Aloss;  /* Expand to a full 8-bit number */
    alpha = (Uint8)temp;

    SDL_Color color = {red, green, blue, alpha};
    return color;

}

//Get all pixels on the window
SDL_Surface* getPixels(SDL_Window* SDLWindow, SDL_Renderer* SDLRenderer) {
    SDL_Surface* saveSurface = NULL;
    SDL_Surface* infoSurface = NULL;
    infoSurface = SDL_GetWindowSurface(SDLWindow);
    if (infoSurface == NULL) {
        std::cerr << "Failed to create info surface from window in saveScreenshotBMP(string), SDL_GetError() - " << SDL_GetError() << "\n";
        return NULL;
    } else {
        unsigned char * pixels = new (std::nothrow) unsigned char[infoSurface->w * infoSurface->h * infoSurface->format->BytesPerPixel];
        if (pixels == 0) {
            std::cerr << "Unable to allocate memory for screenshot pixel data buffer!\n";
            return NULL;
        } else {
            if (SDL_RenderReadPixels(SDLRenderer, &infoSurface->clip_rect, infoSurface->format->format, pixels, infoSurface->w * infoSurface->format->BytesPerPixel) != 0) {
                std::cerr << "Failed to read pixel data from SDL_Renderer object. SDL_GetError() - " << SDL_GetError() << "\n";
                return NULL;
            } else {
                saveSurface = SDL_CreateRGBSurfaceFrom(pixels, infoSurface->w, infoSurface->h, infoSurface->format->BitsPerPixel, infoSurface->w * infoSurface->format->BytesPerPixel, infoSurface->format->Rmask, infoSurface->format->Gmask, infoSurface->format->Bmask, infoSurface->format->Amask);
            }
        }
    }
    return infoSurface;
}

Uint32 get_pixel32( SDL_Surface *surface, int x, int y ) {
    //Convert the pixels to 32 bit 
    Uint32 *pixels = (Uint32 *)surface->pixels; 
    //Get the requested pixel 
    return pixels[ ( y * surface->w ) + x ];
}

SDL_Color getColor(SDL_Surface *surface, int x, int y) {
    auto fmt = surface->format;
    SDL_Color cl;
    SDL_GetRGB(get_pixel32(surface, x,y), fmt, &cl.r, &cl.g, &cl.b);
    cl.a = 255;
    return cl;
}

//Compare two colors
bool compareTwoColors(SDL_Color color1, SDL_Color color2)
{
    if (color1.r == color2.r && color1.g == color2.g && color1.b == color2.b && color1.a == color2.a)
        return true;
    return false;
}

void BoundaryFill4(SDL_Window *win, Vector2D startPoint,Uint32 pixel_format,
                   SDL_Renderer *ren, SDL_Color fillColor, SDL_Color boundaryColor)
{
    auto wind_surface = getPixels(win, ren);
    typedef pair<int,int> P;
    stack<P, deque<P> > st;
    st.push(P(startPoint.x, startPoint.y));
    do {
        int x = get<0>(st.top());
        int y = get<1>(st.top());
        st.pop();
        SDL_Color color = getColor(wind_surface, x,y);
        if (!compareTwoColors(color, fillColor)
         && !compareTwoColors(color, boundaryColor)) {
            SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
            SDL_RenderDrawPoint(ren, x,y);
            //SDL_RenderPresent(ren);
            st.push(P(x,y+1));
            st.push(P(x,y-1));
            st.push(P(x+1,y));
            st.push(P(x-1,y));
        }
    } while (!st.empty());
}

//======================================================================================================================
//=============================================FILLING TRIANGLE=========================================================

int maxIn3(int a, int b, int c) {
    if (a>b) {
        if (a>c) return a;
        return c;
    } else {
        if (b>c) return b;
        return c;
    }
}

int minIn3(int a, int b, int c) {
    if (a<b) {
        if (a<c) return a;
        return c;
    } else {
        if (b<c) return b;
        return c;
    }
}

void swap(Vector2D &a, Vector2D &b) {
    auto t1 = a.x;
    auto t2 = a.y;
    a.x = b.x;
    a.y = b.y;
    b.x = t1;
    b.y = t2;
}

void ascendingSort(Vector2D &v1, Vector2D &v2, Vector2D &v3) {
    if (v1.y > v2.y) swap(v1,v2);
    if (v1.y > v3.y) swap(v1,v3);
    if (v3.y < v2.y) swap(v3,v2);
}

void TriangleFill1(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
    int x1 = minIn3(v1.x, v2.x, v3.x);
    int x2 = maxIn3(v1.x, v2.x, v3.x);
    int y = v1.y;
    for (int x = x1; x<=x2; x++) SDL_RenderDrawPoint(ren, x,y);
}

void TriangleFill2(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
    double x1 = min(v3.x, v2.x);
    double x2 = max(v3.x, v2.x);
    double d1 = (v1.x-v2.x)/(v1.y-v2.y);
    double d2 = (v1.x-v3.x)/(v1.y-v3.y);
    if (v2.x > v3.x) swap(d1,d2);
    for (int y = v2.y; y>=v1.y; y--) {
        for (int x = x1; x<=x2; x++)
            SDL_RenderDrawPoint(ren, x,y);
        x1 -= d1;
        x2 -= d2;
    }
}

void TriangleFill3(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
    double x1 = min(v1.x, v2.x);
    double x2 = max(v1.x, v2.x);
    double d1 = (v3.x-v1.x)/(v3.y-v1.y);
    double d2 = (v3.x-v2.x)/(v3.y-v2.y);
    if (v1.x > v2.x) swap(d1,d2);
    for (int y = v2.y; y<=v3.y; y++) {
        for (int x = x1; x<=x2; x++)
            SDL_RenderDrawPoint(ren, x,y);
        x1 += d1;
        x2 += d2;
    }
}

void TriangleFill4(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
    Vector2D v4 ((v3.x-v1.x)/(v3.y-v1.y)*(v2.y-v1.y) + v1.x, v2.y);
    TriangleFill2(v1,v4,v2, ren,fillColor);
    TriangleFill3(v4,v2,v3, ren,fillColor);
}

void TriangleFill(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
    SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
    ascendingSort(v1,v2,v3);
    if (v1.y == v2.y && v2.y == v3.y) TriangleFill1(v1,v2,v3, ren, fillColor);
    else if (v2.y == v3.y) TriangleFill2(v1,v2,v3, ren, fillColor);
    else if (v1.y == v2.y) TriangleFill3(v1,v2,v3, ren, fillColor);
    else TriangleFill4(v1,v2,v3, ren, fillColor);
}

//======================================================================================================================
//===================================CIRCLE - RECTANGLE - ELLIPSE=======================================================
bool isInsideCircle(int xc, int yc, int R, int x, int y)
{
    return (x-xc)*(x-xc) + (y-yc)*(y-yc) <= R*R;
}

void FillIntersection(int x1,int y1,int x2,int y2, int xc, int yc, int R,
                      SDL_Renderer *ren, SDL_Color fillColor)
{
    for (int x=x1; x<=x2; x++)
        if (isInsideCircle(xc,yc,R, x,y1)) {
            SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
            SDL_RenderDrawPoint(ren, x,y1);
        }
}

void FillIntersectionRectangleCircle(Vector2D vTopLeft, Vector2D vBottomRight, int xc, int yc, int R,
                                     SDL_Renderer *ren, SDL_Color fillColor)
{
    int x1 = vTopLeft.x;
    int x2 = vBottomRight.x;
    int y1 = vTopLeft.y;
    int y2 = vBottomRight.y;
    if (x1>x2) swap(x1,x2);
    if (y1>y2) swap(y1,y2);
    for (int y=y1; y<=y2; y++)
        FillIntersection(x1,y,x2,y, xc,yc,R,ren,fillColor);
}

void RectangleFill(Vector2D vTopLeft, Vector2D vBottomRight, SDL_Renderer *ren, SDL_Color fillColor)
{
    SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
    int x1 = vTopLeft.x;
    int x2 = vBottomRight.x;
    int y1 = vTopLeft.y;
    int y2 = vBottomRight.y;
    if (x1>x2) swap(x1,x2);
    if (y1>y2) swap(y1,y2);
    for (int y=y1; y<=y2; y++)
        for (int x=x1; x<=x2; x++)
            SDL_RenderDrawPoint(ren, x,y);
}

void put4line(int xc, int yc, int x, int y, SDL_Renderer *ren, SDL_Color fillColor)
{
    SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
    for (int xi=xc-x; xi<=xc+x; xi++) {
        SDL_RenderDrawPoint(ren, xi,yc+y);
        SDL_RenderDrawPoint(ren, xi,yc-y);
    }
    for (int xi=xc-y; xi<=xc+y; xi++) {
        SDL_RenderDrawPoint(ren, xi,yc+x);
        SDL_RenderDrawPoint(ren, xi,yc-x);
    }
}

void CircleFill(int xc, int yc, int R, SDL_Renderer *ren, SDL_Color fillColor)
{
    SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	int x=R, y=0,
		p=1-R;
	while (y<x) {
        put4line(xc,yc,x,y,ren,fillColor);
		if (p>0) {
			p+=2*y-2*x+5;
			x--;
		} else p+=2*y+3;
		y++;
	}
}

bool isInsideEllipse(int x,int y,int xc,int yc,int a,int b) {
    return 1.*(x-xc)*(x-xc) / (a*a) + 1.*(y-yc)*(y-yc) / (b*b) <= 1;
}

void put4lineIntersectEllipse(int xc,int yc,int x,int y,int xcE,int ycE,int a,int b, SDL_Renderer* ren, SDL_Color fillColor) {
    for (int xi=xc-x; xi<=xc+x; xi++) {
        if (isInsideEllipse(xi,yc+y, xcE,ycE, a,b))
            SDL_RenderDrawPoint(ren, xi,yc+y);
        if (isInsideEllipse(xi,yc-y, xcE,ycE, a,b))
            SDL_RenderDrawPoint(ren, xi,yc-y);
    }
    for (int xi=xc-y; xi<=xc+y; xi++) {
        if (isInsideEllipse(xi,yc+x, xcE,ycE, a,b))
            SDL_RenderDrawPoint(ren, xi,yc+x);
        if (isInsideEllipse(xi,yc-x, xcE,ycE, a,b))
            SDL_RenderDrawPoint(ren, xi,yc-x);
    }
}

void FillIntersectionEllipseCircle(int xcE, int ycE, int a,int b, int xc, int yc, int R,
                                     SDL_Renderer *ren, SDL_Color fillColor)
{
    SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	int x=R, y=0,
		p=1-R;
	while (y<x) {
        put4lineIntersectEllipse(xc,yc,x,y,xcE,ycE,a,b,ren,fillColor);
		if (p>0) {
			p+=2*y-2*x+5;
			x--;
		} else p+=2*y+3;
		y++;
	}
}

void put4lineIntersectCircle(int xc,int yc,int x,int y,int xc2,int yc2,int R2, SDL_Renderer* ren, SDL_Color fillColor) {
    for (int xi=xc-x; xi<=xc+x; xi++) {
        if (isInsideCircle(xc2,yc2, R2,xi,yc+y))
            SDL_RenderDrawPoint(ren, xi,yc+y);
        if (isInsideCircle(xc2,yc2, R2,xi,yc-y))
            SDL_RenderDrawPoint(ren, xi,yc-y);
    }
    for (int xi=xc-y; xi<=xc+y; xi++) {
        if (isInsideCircle(xc2,yc2, R2,xi,yc+x))
            SDL_RenderDrawPoint(ren, xi,yc+x);
        if (isInsideCircle(xc2,yc2, R2,xi,yc-x))
            SDL_RenderDrawPoint(ren, xi,yc-x);
    }
}

void FillIntersectionTwoCircles(int xc1, int yc1, int R1, int xc2, int yc2, int R2,
                                   SDL_Renderer *ren, SDL_Color fillColor)
{
    SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
    int R = R1;
	int x=R, y=0,
		p=1-R;
	while (y<x) {
        put4lineIntersectCircle(xc1,yc1,x,y,xc2,yc2,R2,ren,fillColor);
		if (p>0) {
			p+=2*y-2*x+5;
			x--;
		} else p+=2*y+3;
		y++;
	}
}

