#include "Clipping.h"

RECT CreateWindow(int l, int r, int t, int b)
{
    RECT rect;
    rect.Left = l;
    rect.Right = r;
    rect.Top = t;
    rect.Bottom = b;

    return rect;
}

CODE Encode(RECT r, Vector2D P)
{
    CODE c = 0;
    if (P.x < r.Left)
        c = c|LEFT;
    if (P.x > r.Right)
        c = c|RIGHT;
    if (P.y < r.Top)
        c = c|TOP;
    if (P.y > r.Bottom)
        c = c|BOTTOM;
    return c;
}

int CheckCase(int c1, int c2)
{
    if (c1 == 0 && c2 == 0)
        return 1;
    if (c1 != 0 && c2 != 0 && (c1&c2) != 0)
        return 2;
    return 3;
}

int getbit(int bit, int pos) {
    return (bit&(1<<pos))>>pos;
}

int CohenSutherland(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
    Q1 = P1;    Q2 = P2;
    CODE cd1 = Encode(r,Q1),
         cd2 = Encode(r,Q2);
    while (CheckCase(cd1,cd2) == 3) {
        ClippingCohenSutherland(r, Q1,Q2);
        cd1 = Encode(r,Q1);
        cd2 = Encode(r,Q2);
    }
    if (CheckCase(cd1,cd2) == 2) return 0;
    return 1;
}

void ClippingCohenSutherland(RECT r, Vector2D &P1, Vector2D &P2) {
    CODE cd = Encode(r, P1);
    if (getbit(cd, 0)) {
        P1.y = P1.y + (P2.y-P1.y) * (r.Left-P1.x) / (P2.x-P1.x);
        P1.x = r.Left;
        return;
    }
    if (getbit(cd, 1)) {
        P1.y = P1.y + (P2.y-P1.y) * (r.Right-P1.x) / (P2.x-P1.x);
        P1.x = r.Right;
        return;
    }
    if (getbit(cd, 2)) {
        P1.x = P1.x + (P2.x-P1.x) * (r.Bottom-P1.y) / (P2.y-P1.y);
        P1.y = r.Bottom;
        return;
    }
    if (getbit(cd, 3)) {
        P1.x = P1.x + (P2.x-P1.x) * (r.Top-P1.y) / (P2.y-P1.y);
        P1.y = r.Top;
        return;
    }
    Vector2D tmp(P1);
    P1.set(P2); P2.set(tmp);
}

int SolveNonLinearEquation(int p, int q, float &t1, float &t2)
{
    if (p == 0)
    {
        if (q < 0)
            return 0;
        return 1;
    }

    if (p > 0)
    {
        float t=(float)q/p;
        if(t2<t)
            return 1;
        if(t<t1)
            return 0;
        t2 = t;
        return 1;
    }

    float t=(float)q/p;
    if(t2<t)
        return 0;
    if(t<t1)
        return 1;
    t1 = t;
    return 1;
}

int LiangBarsky(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2) {
    int dx = P2.x - P1.x,
        dy = P2.y - P1.y;
    float t1, t2;
    int p[4] {dx, -dx, -dy, dy},
        q[4] {r.Right - P1.x,
              P1.x - r.Left,
              P1.y - r.Top,
              r.Bottom - P1.y};
    t1 = 0;
    t2 = 1;
    for (int i=0; i<4; i++) {
        int kq = SolveNonLinearEquation(p[i],q[i],t1,t2);
        if (kq==0) return 0;
    }
    Q1.x = P1.x + t1 * dx;
    Q1.y = P1.y + t1 * dy;
    Q2.x = P1.x + t2 * dx;
    Q2.y = P1.y + t2 * dy;
    return 1;
}
