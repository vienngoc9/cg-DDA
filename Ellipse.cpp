#include "Ellipse.h"
#include <iostream>
#include <cmath>

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, +x+xc,+y+yc);
	SDL_RenderDrawPoint(ren, -x+xc,+y+yc);
	SDL_RenderDrawPoint(ren, -x+xc,-y+yc);
	SDL_RenderDrawPoint(ren, +x+xc,-y+yc);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	// Area 1
	int x=0, y=-b,
		p=4*b*b -4*b*a*a +a*a;
	while (x*b*b <= -y*a*a) {
		Draw4Points(xc,yc,x,y,ren);
		x++;
		p += 4*b*b*(2*x+1);
		if (p>0) {
			y++;
			p += 8*a*a*y;
		}
	}
	// Area 2
	p=(x-.5)*(x-.5)*b*b +y*y*a*a -a*a*b*b;
	p*=4;
	while (y<=0) {
		Draw4Points(xc,yc,x,y,ren);
		y++;
		p += 4*a*a*(2*y+1);
		if (p<0) {
			x++;
			p += 8*b*b*x;
		}
	}
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	// Area 1
	int x=0, y=-b,
		p=b*b -b*a*a +.25*a*a;
	while (x*b*b <= -y*a*a) {
		Draw4Points(xc,yc,x,y,ren);
		x++;
		p += b*b*(2*x+1);
		if (p>0) {
			y++;
			p += 2*a*a*y;
		}
	}
	// Area 2
	p=(x-.5)*(x-.5)*b*b +y*y*a*a -a*a*b*b;
	while (y<=0) {
		Draw4Points(xc,yc,x,y,ren);
		y++;
		p += a*a*(2*y+1);
		if (p<0) {
			x++;
			p += 2*b*b*x;
		}
	}
}
