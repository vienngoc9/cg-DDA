#include "DrawPolygon.h"
#include <iostream>
#include <cmath>
using namespace std;

void NPolygonDraw(int npoly, int xc, int yc, double r, double phi, SDL_Renderer* ren) {
    // npoly: number of the polygon's vertices
    int* x = new int[npoly];
    int* y = new int[npoly];
    double phidelta = 2*M_PI/npoly;
    for (int i=0; i<npoly; i++) {
        x[i] = xc +round(r*cos(phi));
        y[i] = yc -round(r*sin(phi));
        phi += phidelta;
    }
    for (int i=0; i<npoly; i++) {
        Midpoint_Line(x[i],y[i], x[(i+1)%npoly],y[(i+1)%npoly], ren);
    }
}
void ManyNPolygonDraw(int npoly, int xc, int yc, double r, double phi, SDL_Renderer* ren) {
    // npoly: number of the polygon's vertices
    while (r>=1) {
        NPolygonDraw(npoly, xc,yc, r,phi, ren);
        phi += M_PI/npoly;
        r = r*cos(M_PI/npoly);
    }
}
void NStarDraw(int nstar, int step, int xc, int yc, double r, double phi, SDL_Renderer* ren) {
    int* x = new int[nstar];
    int* y = new int[nstar];
    double phidelta = 2*M_PI/nstar;
    for (int i=0; i<nstar; i++) {
        x[i] = xc +round(r*cos(phi));
        y[i] = yc -round(r*sin(phi));
        phi += phidelta;
    }
    for (int i=0; i<nstar; i++) {
        Midpoint_Line(x[i],y[i], x[(i+step)%nstar],y[(i+step)%nstar], ren);
    }
}
double NEmptyStarDraw(int nstar, int step, int xc, int yc, double r, double phi, SDL_Renderer* ren) {
    // nstar: number of the star's vertices
    int* x = new int[nstar];
    int* y = new int[nstar];
    int* sx = new int[nstar];
    int* sy = new int[nstar];
    double phidelta = 2*M_PI/nstar;
    double sphi = phi +phidelta/2;
    double a = M_PI*(nstar%step)/2/nstar;
    double b = M_PI -a -phidelta/2;
    double sr = r *sin(a) /sin(b);
    for (int i=0; i<nstar; i++) {
        x[i] = xc +round(r*cos(phi));
        y[i] = yc -round(r*sin(phi));
        phi += phidelta;
        sx[i] = xc +round(sr*cos(sphi));
        sy[i] = yc -round(sr*sin(sphi));
        sphi += phidelta;
    }
    for (int i=0; i<nstar; i++) {
        Midpoint_Line(x[i],y[i], sx[i],sy[i], ren);
        Midpoint_Line(sx[i],sy[i], x[(i+1)%nstar],y[(i+1)%nstar], ren);
    }
    return sr;
}

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren) {
    NPolygonDraw(3, xc, yc, R, M_PI/2, ren);
}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren) {
    NPolygonDraw(4, xc, yc, R, 0, ren);
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren) {
    NPolygonDraw(5, xc, yc, R, M_PI/2, ren);
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren) {
    NPolygonDraw(6, xc, yc, R, 0, ren);
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren) {
    NStarDraw(5, 2, xc, yc, R, M_PI/2, ren);
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren) {
    NEmptyStarDraw(5, 2, xc, yc, R, M_PI/2, ren);
}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren) {
    NEmptyStarDraw(8, 3, xc, yc, R, M_PI/2, ren);
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren) {
    NEmptyStarDraw(8, 3, xc, yc, R, startAngle, ren);
}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren) {
    int nstar = 5, step = 2;
    double phi = M_PI/2,
           phidelta = M_PI/nstar;
    while (r>=1) {
        r = NEmptyStarDraw(nstar, step, xc, yc, r, phi, ren);
        phi += phidelta;
    }
}
