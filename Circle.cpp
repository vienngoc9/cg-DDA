#include "Circle.h"

void Draw8Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    int new_x;
    int new_y;

    new_x = xc + x;
    new_y = yc + y;
    SDL_RenderDrawPoint(ren, new_x, new_y);
    new_x = xc + x;
    new_y = yc - y;
    SDL_RenderDrawPoint(ren, new_x, new_y);
    new_x = xc - x;
    new_y = yc - y;
    SDL_RenderDrawPoint(ren, new_x, new_y);
    new_x = xc - x;
    new_y = yc + y;
    SDL_RenderDrawPoint(ren, new_x, new_y);
	
    new_x = xc + y;
    new_y = yc + x;
    SDL_RenderDrawPoint(ren, new_x, new_y);
    new_x = xc + y;
    new_y = yc - x;
    SDL_RenderDrawPoint(ren, new_x, new_y);
    new_x = xc - y;
    new_y = yc - x;
    SDL_RenderDrawPoint(ren, new_x, new_y);
    new_x = xc - y;
    new_y = yc + x;
    SDL_RenderDrawPoint(ren, new_x, new_y);
}

void BresenhamDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x=R, y=0,
		p=3-2*R;
	while (y<x) {
		Draw8Points(xc, yc, x, y, ren);
		if (p>0) {
			p+=4*y-4*x+10;
			x--;
		} else p+=4*y+6;
		y++;
	}
}

void MidpointDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x=R, y=0,
		p=1-R;
	while (y<x) {
		Draw8Points(xc, yc, x, y, ren);
		if (p>0) {
			p+=2*y-2*x+5;
			x--;
		} else p+=2*y+3;
		y++;
	}
}
