#include <iostream>
#include <SDL.h>
#include "Bezier.h"

using namespace std;

const int WIDTH  = 800;
const int HEIGHT = 800;

SDL_Event event;

void setColor(SDL_Renderer* ren, SDL_Color cl) {
    SDL_SetRenderDrawColor(ren, cl.r, cl.g, cl.b, cl.a);
}
void centeredRect(SDL_Rect* rect, int cx, int cy, int rect_size) {
    rect->x = cx - rect_size;
    rect->y = cy - rect_size;
    rect->w = rect_size*2;
    rect->h = rect_size*2;
}

int main(int, char**){
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0){
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 20, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL){
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
    //DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

    SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL){
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

    SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

    //YOU CAN INSERT CODE FOR TESTING HERE

    Vector2D p1(22, 215), p2(94, 43), p3(140, 258), p4(213, 150);
    const int rect_size = 6;

    SDL_Rect *rect1 = new SDL_Rect();
    centeredRect(rect1, p1.x,p1.y, rect_size);
    SDL_Rect *rect2 = new SDL_Rect();
    centeredRect(rect2, p2.x,p2.y, rect_size);
    SDL_Rect *rect3 = new SDL_Rect();
    centeredRect(rect3, p3.x,p3.y, rect_size);
    SDL_Rect *rect4 = new SDL_Rect();
    centeredRect(rect4, p4.x,p4.y, rect_size);

    SDL_Color colorCurve = {255, 255, 255, 255}, colorRect = {0, 255, 255, 255};

    setColor(ren, colorRect);
    SDL_RenderDrawRect(ren, rect1);
    SDL_RenderDrawRect(ren, rect2);
    SDL_RenderDrawRect(ren, rect3);
    SDL_RenderDrawRect(ren, rect4);
    setColor(ren, colorCurve);
    DrawCurve3(ren, p1, p2, p3, p4);

    //Take a quick break after all that hard work
    //Quit if happen QUIT event
	bool running = true;
    SDL_Point mouse{0,0};
    int dragged_rect = 0; // the rect being dragged

	while(running)
	{
        //If there's events to handle
		SDL_Event e;
		while(SDL_PollEvent(&e))
		{
			switch(e.type)
			{
				case SDL_QUIT: running = false; break;
                case SDL_KEYDOWN:
                    if (e.key.keysym.sym == 'q') running=0;
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    mouse = {e.button.x, e.button.y};
                    if (SDL_PointInRect(&mouse, rect1)) dragged_rect = 1;
                    if (SDL_PointInRect(&mouse, rect2)) dragged_rect = 2;
                    if (SDL_PointInRect(&mouse, rect3)) dragged_rect = 3;
                    if (SDL_PointInRect(&mouse, rect4)) dragged_rect = 4;
                    break;
                case SDL_MOUSEBUTTONUP:
                    dragged_rect = 0;
                    break;
                case SDL_MOUSEMOTION:
                    if (dragged_rect != 0) {
                        mouse = {e.motion.x, e.motion.y};
                        if (dragged_rect == 1) {
                            p1.x = mouse.x;
                            p1.y = mouse.y;
                            centeredRect(rect1, p1.x,p1.y, rect_size); }
                        if (dragged_rect == 2) {
                            p2.x = mouse.x;
                            p2.y = mouse.y;
                            centeredRect(rect2, p2.x,p2.y, rect_size); }
                        if (dragged_rect == 3) {
                            p3.x = mouse.x;
                            p3.y = mouse.y;
                            centeredRect(rect3, p3.x,p3.y, rect_size); }
                        if (dragged_rect == 4) {
                            p4.x = mouse.x;
                            p4.y = mouse.y;
                            centeredRect(rect4, p4.x,p4.y, rect_size); }
                        SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
                        SDL_RenderClear(ren);
                        setColor(ren, colorRect);
                        SDL_RenderDrawRect(ren, rect1);
                        SDL_RenderDrawRect(ren, rect2);
                        SDL_RenderDrawRect(ren, rect3);
                        SDL_RenderDrawRect(ren, rect4);
                        setColor(ren, colorCurve);
                        DrawCurve3(ren, p1, p2, p3, p4);
                    }
                    break;
			}
        }
        SDL_RenderPresent(ren);
        SDL_Delay(10);
    }

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}
