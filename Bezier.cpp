#include "Bezier.h"
#include <iostream>
using namespace std;

double Bezier2(double t, int* w) {
    double t2 = t*t,
           mt = 1-t,
           mt2 = mt*mt;
    return w[0]*mt2 + w[1]*2*mt*t + w[2]*t2;
}
double Bezier3(double t, int* w) {
    double t2 = t*t,
           t3 = t2*t,
           mt = 1-t,
           mt2 = mt*mt,
           mt3 = mt2*mt;
    return w[0]*mt3 + 3*w[1]*mt2*t + 3*w[2]*mt*t2 + w[3]*t3;
}
double Bezier2der(double t, int* w) {
    // derivative of 2nd order Bezier
    double mt = 1-t;
    return 2*mt*(w[1]-w[0]) +2*(w[1]-w[0]);
}
double Bezier3der(double t, int* w) {
    // derivative of 3nd order Bezier
    double t2 = t*t,
           mt = 1-t,
           mt2 = mt*mt;
    return 3*mt2*(w[1]-w[0]) +6*mt*t*(w[2]-w[1]) +3*t2*(w[3]-w[2]);
}
void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
    int* wx = new int[3];
    int* wy = new int[3];
    wx[0]=p1.x;  wx[1]=p2.x;  wx[2]=p3.x;
    wy[0]=p1.y;  wy[1]=p2.y;  wy[2]=p3.y;
    double t = 0;
    while (t<=1) {
        int x = Bezier2(t,wx),
            y = Bezier2(t,wy);
        SDL_RenderDrawPoint(ren, x,y);
        double dx = Bezier2der(t,wx),
               dy = Bezier2der(t,wy),
               dt = fmin(fabs(1/dx), fabs(1/dy));
        t += dt;
    }
}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
    int* wx = new int[4];
    int* wy = new int[4];
    wx[0]=p1.x;  wx[1]=p2.x;  wx[2]=p3.x;  wx[3]=p4.x;
    wy[0]=p1.y;  wy[1]=p2.y;  wy[2]=p3.y;  wy[3]=p4.y;
    double t = 0;
    while (t<=1) {
        int x = Bezier3(t,wx),
            y = Bezier3(t,wy);
        SDL_RenderDrawPoint(ren, x,y);
        double dx = Bezier3der(t,wx),
               dy = Bezier3der(t,wy),
               dt = fmin(fabs(1/dx), fabs(1/dy));
        t += dt;
    }
}


